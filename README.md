# Bostoko Affiliate #

Bostoko Affiliate Tracking System adalah project untuk sistem marketing Bostoko. Dibangun dengan PHP 5.4 + Yii Framework 1.1.16-dev.

### Requirements ###
This web-apps generated using Yii Framework 1.1.16-dev and you should use at least:

```
#!php

* Yii Framework 1.1.16.
* PHP 5.4.0
* MySQL 5.5 (preferred to use MariaDB as MySQL Replacement) 
```


## First Lunch ##
Before you going to clone and use with your work, please setup a MySQL Database (MariaDB is preferred) in your machine along with database username/password. 

Clone this repo by using

```
git clone https://github.com/agungsijawir/affiliate.git
```

Open main.php in /protected/config/main.php and search 'db' section, edit with your database configuration. Then, you need to import sql schema from /protected/data/schema.mysql.sql into your database.

Open index.php in root of this apps and edit YII Framework location.

Create folder:
```
root/
    /assets
    /protected/runtime

set CHMOD to 775 to these folders
```

If everything setted up, you can access this web apps from http://localhost/yii-eneng-boilerplate

### Login Info ###
There are 2 (two) users in this apps:

```
1. admin / password: admin
2. demo / password: demo
```


Password hash using sha1, you can change this password hash in /protected/config/main.php section modules/user/hash. Please refer php documentation about hash support.

## Version History
v0.1 - 29 Oktober 2014 - Early development stage 
- Initial repository

## Components License
Overall, the following is included in YiiBooster:
*   [YII Framework 1.1.16][yiiframework] - BSD License
*   [Twitter Bootstrap v2.3][twitter-bootstrap]
*   [FontAwesome icons pack](http://fortawesome.github.io/Font-Awesome/) for Twitter Bootstrap.
*   [YiiBooster v3.3.1][yiibooster] by CleverTech, awesome YII Extentions!
*   [Bootplus CSS][bootplus-css] by Marcello Palmitessa (http://twitter.com/aozoralabs), great UI
*   Yii-User by Mikhail Mangushev <mishamx@gmail.com> 
*   Yii-Rights by Christoffer Niska <cniska@live.com>

Private project by @agungsijawir, @imamherlambang & team.

[twitter-bootstrap]: http://twitter.github.com/bootstrap/
[yiiframework]: http://www.yiiframework.com/license/
[bootplus-css]: http://aozora.github.io/bootplus/
[yiibooster]: http://yiibooster-3.clevertech.biz
