<?php
/**
 * Implements features from WebUser (yii-user)
 * https://github.com/mishamx/yii-user
 *
 * eg. for RWebUser from yii-rights
 */
class WebUserBehavior extends CBehavior
{
    public function getId()
    {
        return $this->owner->getState('__id') ? $this->owner->getState('__id') : 0;
    }

    protected function afterLogin($fromCookie)
    {
        parent::afterLogin($fromCookie);
        $this->updateSession();
    }

    public function updateSession()
    {
        $user              = Yii::app()->getModule('user')->user($this->id);

        if ($user == false) return; // TODO?

        $this->owner->name = $user->username;
        $userAttributes    = array(
            'email'        => $user->email,
            'username'     => $user->username,
            'create_at'    => $user->create_at,
            'lastvisit_at' => $user->lastvisit_at,
        );
        foreach ($userAttributes as $attrName => $attrValue) {
            $this->owner->setState($attrName, $attrValue);
        }
    }
}
