<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Bostoko Affiliate',

	// preloading 'log' component
	'preload'=>array('log', 'bootstrap'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.modules.user.UserModule', // needed to load user module first to determine user
		'application.modules.user.models.*',
		'application.modules.user.components.*',
		'application.modules.rights.*',
		'application.modules.rights.components.*',
		'application.modules.rights.components.dataproviders.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool 
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'abc', // You MUST change this password
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
			'generatorPaths' => array(
				'ext.bootstrap.gii'
			),
		), 
		'user'=>array(
			'hash' => 'ripemd160',
			'sendActivationMail' => true,		# send activation email, php mail must be enabled
			'loginNotActiv' => false,			# allow access for non-activated users
			'activeAfterRegister' => false,		# activate user on registration (only sendActivationMail = false)
			'autoLogin' => true,				# automatically login from registration
			'registrationUrl' => array('/user/registration'),
			'recoveryUrl' => array('/user/recovery'),
			'loginUrl' => array('/user/login'),
			'returnUrl' => array('/'),
			'returnLogoutUrl' => array('/user/login'),
			'tableUsers' => 'tbl_users',
			'tableProfiles' => 'tbl_profiles',
			'tableProfileFields' => 'tbl_profiles_fields',
		),
		'rights'=>array(
			'superuserName'=>'Admin',
			'authenticatedName'=>'Authenticated', 
			'enableBizRule'=>false, 			# Whether to enable authorization item business rules. 
			'enableBizRuleData'=>true,			# Whether to enable data for business rules. 
			'displayDescription'=>true,			# Whether to use item description instead of name.
			'baseUrl'=>'/rights',
			'install'=>false,
		),
	),

	// application components
	'components'=>array(
		'user'=>array( 
			'class'=>'RWebUser',
			'behaviors' => array(
				'ext.WebUserBehavior',
			),
			'loginUrl'=>array('/user/login'), 
			'allowAutoLogin'=>true,				# enable cookie-based authentication
		),
		'authManager'=>array(
			'class'=>'RDbAuthManager',
			'connectionID'=>'db',
		),
		'bootstrap' => array(
			'class' => 'ext.bootstrap.components.Bootstrap',
		),
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'caseSensitive'=>false,
			'rules'=>array(
				'login' => 'user/login',
				'signup' => 'user/registration',
				'forgotpassword' => 'user/recovery',
				'myprofile' => 'user/profile',
				'myprofile/edit' => 'user/profile/edit',
				'myprofile/changepassword' => 'user/profile/changepassword',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		'cache'=>array(
			'class'=>'system.caching.CDbCache'
		),
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=affiliate',
			'emulatePrepare' => true,
			'username' => 'affiliate',
			'password' => 'affiliate',
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
		), 
		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),
		'geoip' => array(
			'class' => 'ext.geoip.CGeoIP',
			'filename' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'GeoLiteCity.dat',
			'mode' => 'STANDARD',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'	=>'info@bostoko.com',
		'companyName'	=>'Telkom Indonesia',
		'TMONEY-IP'     =>'118.98.97.45',
		'TMONEY-AUTH'   =>'m3rchantBost0ko',
		'TMONEY-PASS'   =>'Afili@teBost0ko14',
		'geoipdb'       =>dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'GeoLiteCity.dat',
	),
);