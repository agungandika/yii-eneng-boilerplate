<?php

class SampleController extends RController
{
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'rights', // perform access control using Rights for CRUD operations
		);
	}

	public function allowedActions() {
		return 'index';
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionInfo()
	{
		$this->render('info');
	}

	public function actionPage()
	{
		$this->render('page');
	}
}