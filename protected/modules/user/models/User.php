<?php

class User extends CActiveRecord
{
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const STATUS_BANNED=-1;
	
	//TODO: Delete for next version (backward compatibility)
	const STATUS_BANED=-1;
	

	// for rights!
	public $user_role;

	/**
	 * The followings are the available columns in table 'users':
	 * @var integer $id
	 * @var string $username
	 * @var string $password
	 * @var string $email
	 * @var string $activkey
	 * @var integer $createtime
	 * @var integer $lastvisit
	 * @var integer $superuser
	 * @var string $userrole
	 * @var integer $status
	* @var timestamp $create_at
	* @var timestamp $lastvisit_at
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Yii::app()->getModule('user')->tableUsers;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.CConsoleApplication
		return ((get_class(Yii::app())=='CConsoleApplication' || (get_class(Yii::app())!='CConsoleApplication' && Yii::app()->getModule('user')->isAdmin()))?array(
			array('username', 'length', 'max'=>20, 'min' => 3,'message' => UserModule::t("Username salah.")),
			array('password', 'length', 'max'=>128, 'min' => 4,'message' => UserModule::t("Password salah (minimal 4 karakter).")),
			array('email', 'email'),
			array('fname, lname, alamat', 'length', 'max'=>128),
			array('kota, provinsi, no_telp', 'length', 'max'=>64),
			array('username', 'unique', 'message' => UserModule::t("Mohon maaf, username ini sudah digunakan.")),
			array('email', 'unique', 'message' => UserModule::t("E-Mail ini sudah didaftarkan milik member lain.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => UserModule::t("Username hanya berupa huruf dan angka saja (A-z0-9).")),
			array('status', 'in', 'range'=>array(self::STATUS_NOACTIVE,self::STATUS_ACTIVE,self::STATUS_BANNED)),
			array('superuser', 'in', 'range'=>array(0,1)),
			array('create_at', 'default', 'value' => date('Y-m-d H:i:s'), 'setOnEmpty' => true, 'on' => 'insert'),
			array('lastvisit_at', 'default', 'value' => '0000-00-00 00:00:00', 'setOnEmpty' => true, 'on' => 'insert'),
			array('fname, lname, username, email, superuser, status', 'required'),
			array('superuser, status', 'numerical', 'integerOnly'=>true),
			array('id, username, password, email, activkey, create_at, lastvisit_at, superuser, status, fname, lname', 'safe', 'on'=>'search'),
		):((Yii::app()->user->id==$this->id)?array(
			array('fname, lname, username, email', 'required'),
			array('fname, lname, alamat', 'length', 'max'=>128),
			array('kota, provinsi, no_telp', 'length', 'max'=>64),
			array('username', 'length', 'max'=>20, 'min' => 3,'message' => UserModule::t("Username salah.")),
			array('password', 'length', 'max'=>128, 'min' => 4,'message' => UserModule::t("Password salah (minimal 4 karakter).")),
			array('email', 'email'),
			array('user_role','required'),
			array('username', 'unique', 'message' => UserModule::t("Mohon maaf, username ini sudah digunakan.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => UserModule::t("Username hanya berupa huruf dan angka saja (A-z0-9).")),
			array('email', 'unique', 'message' => UserModule::t("E-Mail ini sudah didaftarkan milik member lain.")),
		):array()));
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		$relations = Yii::app()->getModule('user')->relations;
		if (!isset($relations['profile']))
			$relations['profile'] = array(self::HAS_ONE, 'Profile', 'user_id');
		return $relations;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => UserModule::t("ID"),
			'username'=>UserModule::t("Username"),
			'password'=>UserModule::t("Password"),
			'verifyPassword'=>UserModule::t("Konfirmasi Password"),
			'email'=>UserModule::t("E-mail"),
			'verifyCode'=>UserModule::t("Verification Code"),
			'activkey' => UserModule::t("activation key"),
			'createtime' => UserModule::t("Registration date"),
			'create_at' => UserModule::t("Registration date"),
			
			'lastvisit_at' => UserModule::t("Last visit"),
			'superuser' => UserModule::t("Superuser"),
			'role_user' => UserModule::t("User Role"),
			'status' => UserModule::t("Status"),
			'fname' => 'Nama Depan',
			'lname' => 'Nama Belakang',
			'alamat' => 'Alamat Lengkap',
			'kota' => 'Kota',
			'provinsi' => 'Provinsi',
			'no_telp' => 'No Telp',

		);
	}
	
	public function scopes()
	{
	   return array(
		  'active'=>array(
			 'condition'=>'status='.self::STATUS_ACTIVE,
		  ),
		  'notactive'=>array(
			 'condition'=>'status='.self::STATUS_NOACTIVE,
		  ),
		  'banned'=>array(
			 'condition'=>'status='.self::STATUS_BANNED,
		  ),
		  'superuser'=>array(
			 'condition'=>'superuser=1',
		  ),
		  'notsafe'=>array(
		  	'select' => 'id, username, password, email, activkey, create_at, lastvisit_at, superuser, status, fname, lname, alamat, kota, provinsi, no_telp',
		  ),
	   );
	}
	
	public function defaultScope()
	{
	   return CMap::mergeArray(Yii::app()->getModule('user')->defaultScope,array(
		  'alias'=>'user',
		  'select' => 'user.id, user.username, user.email, user.create_at, user.lastvisit_at, user.superuser, user.status, user.fname, user.lname, user.alamat, user.kota, user.provinsi, user.no_telp',
	   ));
	}
	
	public static function itemAlias($type,$code=NULL) {
		$_items = array(
			'UserStatus' => array(
				self::STATUS_NOACTIVE => UserModule::t('Not active'),
				self::STATUS_ACTIVE => UserModule::t('Active'),
				self::STATUS_BANNED => UserModule::t('Banned'),
			),
			'AdminStatus' => array(
				'0' => UserModule::t('No'),
				'1' => UserModule::t('Yes'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}
	
/**
	* Retrieves a list of models based on the current search/filter conditions.
	* @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	*/
	public function search()
	{
	   // Warning: Please modify the following code to remove attributes that
	   // should not be searched.

	   $criteria=new CDbCriteria;
	   
	   $criteria->compare('id',$this->id);
	   $criteria->compare('username',$this->username,true);
	   $criteria->compare('password',$this->password);
	   $criteria->compare('email',$this->email,true);
	   $criteria->compare('activkey',$this->activkey);
	   $criteria->compare('create_at',$this->create_at);
	   $criteria->compare('lastvisit_at',$this->lastvisit_at);
	   $criteria->compare('superuser',$this->superuser);
	   $criteria->compare('status',$this->status);

	   return new CActiveDataProvider(get_class($this), array(
		  'criteria'=>$criteria,
	   	'pagination'=>array(
				'pageSize'=>Yii::app()->getModule('user')->user_page_size,
			),
	   ));
	}

	public function getCreatetime() {
	   return strtotime($this->create_at);
	}

	public function setCreatetime($value) {
	   $this->create_at=date('Y-m-d H:i:s',$value);
	}

	public function getLastvisit() {
	   return strtotime($this->lastvisit_at);
	}

	public function setLastvisit($value) {
	   $this->lastvisit_at=date('Y-m-d H:i:s',$value);
	}

	public function afterSave() {
	   if (get_class(Yii::app())=='CWebApplication'&&Profile::$regMode==false) {
		  Yii::app()->user->updateSession();
	   }
	   return parent::afterSave();
	}
}