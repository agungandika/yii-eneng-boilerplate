<?php
/**
 * RegistrationForm class.
 * RegistrationForm is the data structure for keeping
 * user registration form data. It is used by the 'registration' action of 'UserController'.
 */
class RegistrationForm extends User {
	public $verifyPassword;
	public $verifyCode;
	
	public function rules() {
		$rules = array(
			array('fname, lname, username, password, verifyPassword, email', 'required', 'message' => '{attribute} wajib diisi.'),
			array('username', 'length', 'max'=>20, 'min' => 3,'message' => UserModule::t("Username salah.")),
			array('password', 'length', 'max'=>128, 'min' => 4,'message' => UserModule::t("Password salah (minimal 4 karakter).")),
			array('email', 'email'),
			array('username', 'unique', 'message' => UserModule::t("Mohon maaf, username ini sudah digunakan.")),
			array('email', 'unique', 'message' => UserModule::t("E-Mail ini sudah didaftarkan milik member lain.")),
			//array('verifyPassword', 'compare', 'compareAttribute'=>'password', 'message' => UserModule::t("Retype Password is incorrect.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' => UserModule::t("Username hanya berupa huruf dan angka saja (A-z0-9).")),
		);
		if (!(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')) {
			array_push($rules,array('verifyCode', 'captcha', 'allowEmpty'=>!UserModule::doCaptcha('registration')));
		}
		
		array_push($rules,array('verifyPassword', 'compare', 'compareAttribute'=>'password', 'message' => UserModule::t("Konfirmasi Password tidak cocok.")));
		return $rules;
	}
	
}