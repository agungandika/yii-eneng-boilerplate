<?php $form=$this->beginWidget('ext.bootstrap.widgets.TbActiveForm', array(
	'id'=>'user-form', 
	'type' => 'horizontal',
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
	'enableAjaxValidation'=>true,
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
));
?>

	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

	<div class="row-fluid">
		<div class="span6">
		<?php echo $form->textFieldRow($model,'username',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'username'); ?> 

		<?php echo $form->passwordFieldRow($model,'password',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'password'); ?>

		<?php echo $form->textFieldRow($model,'email',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>

		<?php echo $form->select2Row($model,'superuser', array('data'=>User::itemAlias('AdminStatus'))); ?>
		<?php echo $form->error($model,'superuser'); ?>

		<div class="control-group">
			<?php echo $form->labelEx($model,'role_user', array('class'=>'control-label')); ?>
			<div class="controls">			
				<?php
				if (Yii::app()->user->isSuperuser && $model->isNewRecord) {
					$all_roles=new RAuthItemDataProvider('roles', array('type'=>2));
					$data_roles=$all_roles->fetchData(); 
				?> 
		        	<?php echo CHtml::dropDownList("role_user",'',CHtml::listData($data_roles,'name','name'));?> 
				<?php } else { ?>
					<?php
						$roles_data = '';
						$roles=Rights::getAssignedRoles($model->id);
						foreach($roles as $role) $roles_data .= $role->name . " ";

						echo CHtml::textField("role_user", $roles_data, array('disabled'=>'true'));
					?>
					<?php echo CHtml::link('<i class="icon-edit"></i>', array('/rights/assignment/user/', 'id'=>$model->id), array('class'=>'btn','data-toggle'=>'tooltip', 'title'=>'Click to edit User Roles.', 'data-placement' => 'right')); ?>
				<?php } // endif ?>
			</div>
		</div>

		<?php echo $form->select2Row($model,'status', array('data'=>User::itemAlias('UserStatus'))); ?>
		<?php echo $form->error($model,'status'); ?>

		<?php 
				$profileFields=Profile::getFields();
				if ($profileFields) {
					foreach($profileFields as $field) {
					?> 
				<?php 
				if ($widgetEdit = $field->widgetEdit($profile)) {
					echo $widgetEdit;
				} elseif ($field->range) {
					echo $form->dropDownListRow($profile,$field->varname,Profile::range($field->range));
				} elseif ($field->field_type=="TEXT") {
					echo CHtml::activeTextAreaRow($profile,$field->varname,array('rows'=>6, 'cols'=>50));
				} else {
					echo $form->textFieldRow($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
				}
				 ?>
				<?php echo $form->error($profile,$field->varname); ?>

					<?php
					}
				}
		?>
		</div>

		<div class="span6">
			<?php echo $form->errorSummary(array($model,$profile)); ?>
		</div>
	</div>
	<div class="form-actions">
		<?php echo CHtml::submitButton($model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save'), array('class' => 'btn btn-primary')); ?>
		<?php echo CHtml::link(UserModule::t('Cancel'), array('/user/admin/admin'), array('class' => 'btn')); ?>
	</div>

<?php $this->endWidget(); ?>