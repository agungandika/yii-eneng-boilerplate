<?php
$this->breadcrumbs=array(
	(UserModule::t('Users'))=>array('admin'),
	$model->username=>array('view','id'=>$model->id),
	(UserModule::t('Update')),
);
$this->menu=array(
    array('label'=>UserModule::t('Create User'), 'url'=>array('create'), 'icon'=>'icon-file'),
    '---',
    array('label'=>UserModule::t('View User'), 'url'=>array('view','id'=>$model->id), 'icon' => 'icon-zoom-in'),
    array('label'=>UserModule::t('Delete User'), 'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>UserModule::t('Are you sure to delete this user?')), 'icon'=>'icon-remove'),
    '---',
    array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin'), 'icon' => 'icon-user'),
    array('label'=>UserModule::t('Manage Fields'), 'url'=>array('profilefield/admin'), 'icon' => 'icon-th-list'),
    // array('label'=>UserModule::t('List User'), 'url'=>array('/user')),
);
?>

<h1><?php echo  UserModule::t('Update User')." `".$model->username."`"; ?></h1>

<?php
	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile));
?>