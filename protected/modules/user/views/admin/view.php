<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('admin'),
	$model->username,
);


$this->menu=array(
	array('label'=>UserModule::t('Create User'), 'url'=>array('create'), 'icon'=>'icon-file'),
	'---',
	array('label'=>UserModule::t('Assign Roles/Task'), 'url'=>array('/rights/assignment/user/', 'id'=>$model->id), 'icon'=>'icon-hand-right'),
	array('label'=>UserModule::t('Update User'), 'url'=>array('update','id'=>$model->id), 'icon' => 'icon-edit'),
	array('label'=>UserModule::t('Delete User'), 'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>UserModule::t('Are you sure to delete this user?')), 'icon'=>'icon-remove'),
	'---',
	array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin'), 'icon' => 'icon-user'),
	array('label'=>UserModule::t('Manage Fields'), 'url'=>array('profilefield/admin'), 'icon' => 'icon-th-list'),
    // array('label'=>UserModule::t('List User'), 'url'=>array('/user')),
);
?>
<h1><?php echo UserModule::t('View User').' "'.$model->username.'"'; ?></h1>

<?php
 	// getting user roles
	$user_role_data = '';
	$roles = Rights::getAssignedRoles($model->id);
	if (sizeof($roles) < 1)
		$user_role_data = '*** No roles / task assigned with this user ***';
	else if (sizeof($roles) > 1)
		foreach($roles as $role) $user_role_data .= $role->name . ', ';
	else
		foreach($roles as $role) $user_role_data .= $role->name . ' ';

	$attributes = array(
		'id',
		'username',
	);
	
	$profileFields=Profilefield::model()->forOwner()->sort()->findAll();
	if ($profileFields) {
		foreach($profileFields as $field) {
			array_push($attributes,array(
					'label' => UserModule::t($field->title),
					'name' => $field->varname,
					'type'=>'raw',
					'value' => (($field->widgetView($model->profile))?$field->widgetView($model->profile):(($field->range)?Profile::range($field->range,$model->profile->getAttribute($field->varname)):$model->profile->getAttribute($field->varname))),
				));
		}
	}
	
	array_push($attributes,
		// 'password', 
		array(
			'name'=>'email',
			'type'=>'raw',
			'value'=>CHtml::mailto(CHtml::encode($model->email)),
        ),
		'activkey',
		'create_at',
		'lastvisit_at',
		array(
			'name' => 'superuser',
			'value' => User::itemAlias("AdminStatus",$model->superuser),
		),
		array(
			'name' => 'status',
			'value' => User::itemAlias("UserStatus",$model->status),
		),
		array(
			'label'=>'User Assigned Roles',
			'value'=> $user_role_data,
		)
	);
	
	$this->widget('ext.bootstrap.widgets.TbDetailView', array(
		'data'=>$model,
		'attributes'=>$attributes,
	));
?>

<br/>
<a href="<?php echo $this->createUrl('/rights/assignment/user/id/' . $model->id); ?>" class="btn btn-primary">Assign Task and Roles for this user</a>