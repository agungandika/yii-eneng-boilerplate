<?php $form=$this->beginWidget('ext.bootstrap.widgets.TbActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'type'=> 'horizontal',
)); ?>
    <div class="row-fluid">
        <div class="span6">
        <?php echo $form->textFieldRow($model,'id'); ?> 
        <?php echo $form->textFieldRow($model,'username',array('size'=>20,'maxlength'=>20)); ?>
        <?php echo $form->textFieldRow($model,'email',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->textFieldRow($model,'activkey',array('size'=>60,'maxlength'=>128)); ?>
        </div>
        <div class="span6">
        <?php echo $form->textFieldRow($model,'create_at'); ?>
        <?php echo $form->textFieldRow($model,'lastvisit_at'); ?>
        <?php echo $form->dropDownListRow($model,'superuser',$model->itemAlias('AdminStatus'),  array('prompt'=>'--- Select Admin Status ---')); ?>
        <?php echo $form->dropDownListRow($model,'status',$model->itemAlias('UserStatus'), array('prompt'=>'--- Select User Status ---')); ?>
        </div>
    </div>
    <div class="form-actions" style="height: 20px; padding-top: 10px;">
        <?php echo CHtml::submitButton(UserModule::t('Search'), array('class' => 'btn btn-primary')); ?>
        <?php echo CHtml::resetButton(UserModule::t('Reset'), array('class' => 'btn')); ?>
    </div>
<?php $this->endWidget(); ?>