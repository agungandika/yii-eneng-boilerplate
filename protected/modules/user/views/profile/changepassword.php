<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change password");
$this->breadcrumbs=array(
	UserModule::t("My Profile") => array('/user/profile'),
	UserModule::t("Change password"),
);
$this->menu=array(
	((UserModule::isAdmin())
		?array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin'), 'icon' => 'icon-user')
		:array()),
    // array('label'=>UserModule::t('List User'), 'url'=>array('/user')),
    array('label'=>UserModule::t('My Profile'), 'url'=>array('/user/profile'), 'icon' => 'icon-heart'),
    array('label'=>UserModule::t('Update Profile'), 'url'=>array('edit'), 'icon' => 'icon-user'),
    '---',
    array('label'=>UserModule::t('Logout'), 'url'=>array('/user/logout'), 'linkOptions'=>array('confirm'=>UserModule::t('Are you sure want to Logout?')), 'icon' => 'icon-off'),
);
?>

<h1><?php echo UserModule::t("Change password"); ?></h1>

<div class="form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.TbActiveForm', array(
	'id'=>'changepassword-form',
	'enableAjaxValidation'=>true,
	'type'=>'horizontal',
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
	<?php echo $form->errorSummary($model); ?>
	

	<?php echo $form->passwordFieldRow($model,'oldPassword', array('data-toggle'=>'tooltip', 'title'=>UserModule::t("Please enter your old password."), 'data-placement' => 'right')); ?>
	<?php echo $form->error($model,'oldPassword'); ?>

	<?php echo $form->passwordFieldRow($model,'password', array('data-toggle'=>'tooltip', 'title'=>UserModule::t("Minimal password length 4 symbols."), 'data-placement' => 'right')); ?>
	<?php echo $form->error($model,'password'); ?>
	
	<?php echo $form->passwordFieldRow($model,'verifyPassword', array('data-toggle'=>'tooltip', 'title'=>UserModule::t("Please re-type password. Minimal password length 4 symbols."), 'data-placement' => 'right')); ?>
	<?php echo $form->error($model,'verifyPassword'); ?>
	
	<div class="form-actions submit">
	<?php echo CHtml::submitButton(UserModule::t("Save"), array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->