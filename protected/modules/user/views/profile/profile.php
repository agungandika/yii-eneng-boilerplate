<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Profile");
$this->breadcrumbs=array(
	UserModule::t("My Profile"),
);
$this->menu=array(
	((UserModule::isAdmin())
		?array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin'), 'icon' => 'icon-user')
		:array()),
    array('label'=>UserModule::t('Update My Profile'), 'url'=>array('edit'), 'icon' => 'icon-pencil'),
    array('label'=>UserModule::t('Change password'), 'url'=>array('changepassword'), 'icon' => 'icon-edit'),
    '---',
    array('label'=>UserModule::t('Logout'), 'url'=>array('/user/logout'), 'linkOptions'=>array('confirm'=>UserModule::t('Are you sure want to Logout?')), 'icon' =>'icon-off'),
);
?><h1><?php echo UserModule::t('My Profile'); ?></h1>

<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
<div class="success">
	<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
</div>
<?php endif; ?>

<?php 
	$this->widget('ext.bootstrap.widgets.TbDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'username',
			'email',
			'create_at',
			'lastvisit_at',
			array(
				'name' => 'status',
				'type' => 'raw',
				'value' => CHtml::encode(User::itemAlias("UserStatus",$model->status)),
			),
		),
	));
?>
<table class="detail-view table table-striped table-condensed">
	<?php 
		$profileFields=Profilefield::model()->forOwner()->sort()->findAll();
		if ($profileFields) {
			foreach($profileFields as $field) {
				//echo "<pre>"; print_r($profile); die();
			?>
	<tr>
		<th><?php echo CHtml::encode(UserModule::t($field->title)); ?></th>
    	<td><?php echo (($field->widgetView($profile))?$field->widgetView($profile):CHtml::encode((($field->range)?Profile::range($field->range,$profile->getAttribute($field->varname)):$profile->getAttribute($field->varname)))); ?></td>
	</tr>
			<?php
			}//$profile->getAttribute($field->varname)
		}
	?>
</table>
