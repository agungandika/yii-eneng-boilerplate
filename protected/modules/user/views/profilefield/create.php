<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('/user/admin'),
	UserModule::t('Profile Fields')=>array('admin'),
	UserModule::t('Create'),
);
$this->menu=array(
    array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin'), 'icon'=>'icon-user'),
    array('label'=>UserModule::t('Manage Fields'), 'url'=>array('profilefield/admin'), 'icon' => 'icon-th-list'),
);
?>
<h1><?php echo UserModule::t('Create Profile Field'); ?></h1>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>