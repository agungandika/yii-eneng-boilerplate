<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('/user/admin'),
	UserModule::t('Profile Fields')=>array('admin'),
	UserModule::t($model->title),
);
$this->menu=array(
    array('label'=>UserModule::t('Create New Field'), 'url'=>array('create'), 'icon'=>'icon-file'),
	'---',
    array('label'=>UserModule::t('Update Data'), 'url'=>array('update','id'=>$model->id), 'icon' => 'icon-edit'),
    array('label'=>UserModule::t('Delete Data'), 'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>UserModule::t('Are you sure to delete this item?')), 'icon'=>'icon-remove'),
    '---',
	array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin'), 'icon' => 'icon-user'),
	array('label'=>UserModule::t('Manage Fields'), 'url'=>array('profilefield/admin'), 'icon' => 'icon-th-list'),
);
?>
<h1><?php echo UserModule::t('View Profile Field #').$model->varname; ?></h1>

<?php $this->widget('ext.bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'varname',
		'title',
		'field_type',
		'field_size',
		'field_size_min',
		'required',
		'match',
		'range',
		'error_message',
		'other_validator',
		'widget',
		'widgetparams',
		'default',
		'position',
		'visible',
	),
)); ?>