<?php
$this->breadcrumbs=array(
    UserModule::t('Users')=>array('/user/admin'),
	UserModule::t('Profile Fields')=>array('admin'),
	$model->title=>array('view','id'=>$model->id),
	UserModule::t('Update'),
);
$this->menu=array(
    array('label'=>UserModule::t('Create New Field'), 'url'=>array('create'), 'icon'=>'icon-file'),
    '---',
    array('label'=>UserModule::t('View Data'), 'url'=>array('view','id'=>$model->id), 'icon' => 'icon-zoom-in'),
    array('label'=>UserModule::t('Delete Data'), 'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>UserModule::t('Are you sure to delete this item?')), 'icon'=>'icon-remove'),
    '---',
    array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin'), 'icon' => 'icon-user'),
    array('label'=>UserModule::t('Manage Fields'), 'url'=>array('profilefield/admin'), 'icon' => 'icon-th-list'),
);
?>

<h1><?php echo UserModule::t('Update Profile Field `').$model->varname.'`'; ?></h1>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>