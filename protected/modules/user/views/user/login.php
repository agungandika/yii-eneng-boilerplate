<?php
$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
$this->breadcrumbs=array(
	UserModule::t("Login"),
);
?>



<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

<div class="alert">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
</div>

<?php endif; ?>
<div class="row-fluid">
	<div class="span12">
		<?php /** @var TbActiveForm $form */
			$form = $this->beginWidget(
				'ext.bootstrap.widgets.TbActiveForm',
				array(
					'id' => 'loginForm',
					'type' => 'horizontal', 
					// 'enableClientValidation'=>true,
					'clientOptions'=>array(
						'validateOnSubmit'=>true,
					), 
				)
			);
		?>
		<div class="card">
			<h3 class="card-heading simple"><?php echo UserModule::t("Login"); ?></h3>
			<div class="card-body">
				<p><?php echo UserModule::t("Please fill out the following form with your login credentials:"); ?></p>

				<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
				<?php echo CHtml::errorSummary($model); ?>
				<?php echo $form->textFieldRow($model,'username', array('data-toggle'=>'tooltip', 'title'=>UserModule::t("Please enter your username or email."), 'data-placement' => 'right')); ?>

				<?php echo $form->passwordFieldRow($model,'password', array('data-toggle'=>'tooltip', 'title'=>UserModule::t("Please enter your password."), 'data-placement' => 'right')); ?>

				<!-- captcha -->
				<?php if (UserModule::doCaptcha('login')): ?>
					<?php echo $form->textFieldRow($model,'verifyCode', array('data-toggle'=>'tooltip', 'title'=>UserModule::t("Masukkan huruf verifikasi di atas.") . ' ' . UserModule::t("Huruf besar / kecil disamakan."), 'data-placement' => 'right')); ?>
					<div class="control-group">
						<div class="controls">
							<?php $this->widget('CCaptcha', array('buttonOptions' => array('class'=>'btn'))); ?>
						</div>
					</div>
				<?php endif; ?>
				<!-- end of captcha -->

				<div class="control-group">
					<div class="controls">
						<?php echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl); ?> | <?php echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
					</div>
				</div>
				<?php echo $form->checkBoxRow($model,'rememberMe', array('data-toggle'=>'tooltip', 'title'=>UserModule::t("Check this option only if you use private computer."), 'data-placement' => 'right')); ?>
			

			</div>
		</div>

		<div class="form-actions submit">
			<?php echo CHtml::submitButton(UserModule::t("Login"), array('class' => 'btn btn-primary')); ?>
			&nbsp;
		</div>
		<?php $this->endWidget(); ?>
	</div>
</div>