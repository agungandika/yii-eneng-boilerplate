<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Registration");
$this->breadcrumbs=array(
	UserModule::t("Registration"),
);
?>

<h2><?php echo UserModule::t("Pendaftaran Affiliasi"); ?></h2>

<?php if(Yii::app()->user->hasFlash('registration')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('registration'); ?>
</div>
<?php else: ?>

 
<?php $form=$this->beginWidget('ext.bootstrap.widgets.TbActiveForm', array(
	'id'=>'registration-form',
	'enableAjaxValidation'=>true,
	// 'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'type' => 'horizontal',
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note"><?php echo UserModule::t('Isian dengan tanda <span class="required">*</span> wajib diisi.'); ?></p>

	<?php echo $form->errorSummary(array($model), "Kesalahan Pengisian Form Registrasi"); ?>

	<div class="card">
		<h3 class="card-heading simple">Informasi Akun</h3>
		<div class="card-body">
			<?php echo $form->textFieldRow($model,'fname', array('class'=>'span9')); ?>
			<?php echo $form->textFieldRow($model,'lname', array('class'=>'span9')); ?>
			<?php echo $form->textFieldRow($model,'email', array('class'=>'span9')); ?>
			<?php echo $form->textFieldRow($model,'username', array('class'=>'span9')); ?>

			<?php echo $form->passwordFieldRow($model,'password', array('class'=>'span9', 'data-toggle'=>'tooltip', 'title'=>UserModule::t("Minimal password length 4 symbols."), 'data-placement' => 'right')); ?>
			<?php echo $form->passwordFieldRow($model,'verifyPassword', array('class'=>'span9', 'data-toggle'=>'tooltip', 'title'=>UserModule::t("Please re-type password. Minimal password length 4 symbols."), 'data-placement' => 'right')); ?>

			<?php echo $form->textAreaRow($model, 'alamat', array('class'=>'span9', 'rows'=>'4')); ?>


			<?php if (UserModule::doCaptcha('registration')): ?>
				<div class="control-group"><div class="controls">
						<?php $this->widget('CCaptcha'); ?>
					</div></div>
				<?php echo $form->textFieldRow($model,'verifyCode', array('data-toggle'=>'tooltip', 'title'=>UserModule::t("Please enter the letters as they are shown in the image above.") . ' ' . UserModule::t("Letters are not case-sensitive."), 'data-placement' => 'right')); ?>
				<?php echo $form->error($model,'verifyCode'); ?>
			<?php endif; ?>
		</div>
	</div>



	
	<div class="form-actions submit">
		<?php echo CHtml::submitButton(UserModule::t("Daftar"), array('class' => 'btn btn-primary')); ?>
		<?php echo CHtml::resetButton(UserModule::t("Kosongkan"), array('class' => 'btn')); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
<?php endif; ?>