	<?php if( Yii::app()->user->hasFlash('RightsSuccess')===true ):?>
	    <div class="alert alert-success">
	    	<button type="button" class="close" data-dismiss="alert">&times;</button>
	        <?php echo Yii::app()->user->getFlash('RightsSuccess'); ?>
	    </div>
	<?php endif; ?>

	<?php if( Yii::app()->user->hasFlash('RightsError')===true ):?>
	    <div class="alert alert-error">
	    	<button type="button" class="close" data-dismiss="alert">&times;</button>
	        <?php echo Yii::app()->user->getFlash('RightsError'); ?>
	    </div>
	<?php endif; ?>