<?php $this->beginContent(Rights::module()->appLayout); ?>

<div id="rights" class="row-fluid">

	<div id="content">
		<div class="span10">
			<?php $this->renderPartial('/_flash'); ?>
			<?php echo $content; ?>
		</div>

		<div class="span2">
			<?php if( $this->id!=='install' ): ?> 
				<?php $this->renderPartial('/_menu'); ?> 
			<?php endif; ?>
		</div>
	</div><!-- content -->
</div>

<?php $this->endContent(); ?>