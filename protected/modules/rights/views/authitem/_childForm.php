<div class="form">

<?php $form=$this->beginWidget('ext.bootstrap.widgets.TbActiveForm'); ?>
	 
	<?php echo $form->select2Row($model, 'itemname', array('asDropDownList' => true, 'data'=>$itemnameSelectOptions)); ?>
	<?php echo $form->error($model, 'itemname'); ?>
 
	 
		<?php echo CHtml::submitButton(Rights::t('core', 'Add'), array('class'=>'btn btn-primary')); ?>
 

<?php $this->endWidget(); ?>

</div>