<?php $form=$this->beginWidget('ext.bootstrap.widgets.TbActiveForm'); ?>
		<?php echo $form->select2Row($model, 'itemname', array('asDropDownList' => true, 'data'=>$itemnameSelectOptions)); ?>
		<?php echo $form->error($model, 'itemname'); ?>

	<div class="form-actions">
		<?php echo CHtml::submitButton(Rights::t('core', 'Assign'), array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>