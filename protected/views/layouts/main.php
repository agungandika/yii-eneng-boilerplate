<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?php echo CHtml::encode($this->pageTitle); ?> &middot; <?php echo CHtml::encode(Yii::app()->name); ?></title>
		<meta name="viewport" content="width=device-width; initial-scale=0.8; maximum-scale=1.5; user-scalable=0.8;">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="description" content="<?php echo CHtml::encode(Yii::app()->name); ?>">
		<meta name="author" content="Agung Andika (agung@andromega.net)">

		<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true); ?>/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getBaseUrl(true); ?>/css/fonts.css">
		<!--[if IE 7]>
		<link rel="stylesheet" href="<?php echo Yii::app()->getBaseUrl(true); ?>/css/font-awesome-ie7.min.css">
		<![endif]-->

		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getBaseUrl(true); ?>/css/bootplus.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getBaseUrl(true); ?>/css/customs.css">
		<link rel="shortcut icon" href="<?php echo Yii::app()->getBaseUrl(true); ?>/images/icon.png">
	</head>

	<body>
		<div id="wrap">
			<?php
				if (!Yii::app()->user->isGuest)
					{
						if (Yii::app()->user->checkAccess('Admin'))
						{
							$menu_user = array('label' => 'Welcome, ' .
								Yii::app()->getModule('user')->user(Yii::app()->user->Id)->fname . ' ' .
								Yii::app()->getModule('user')->user(Yii::app()->user->Id)->lname,
								'url' => '', // doesn't link to any page
								'icon' =>'icon-user',
								'visible'	=> !Yii::app()->user->isGuest,
								'items'	=> array(
									array('label' => 'Administrations', 'visible' => UserModule::isAdmin()),
									array('label' => 'User Managements', 'url' => array('/user/admin'), 'icon' => 'icon-user', 'visible' => UserModule::isAdmin()),
									array('label' => 'Right Managements', 'url' => array('/rights'), 'icon' => 'icon-tasks', 'visible' => UserModule::isAdmin()),
									array('label' => 'Options'),
									array('label' => 'My Profile', 'icon' => 'icon-heart', 'url' => array('/user/profile')),
									array('label' => 'Change Password', 'icon' => 'icon-edit', 'url' => array('/user/profile/changepassword')),
									'---',
									array('label' => 'Logout ('.Yii::app()->user->name.')', 'icon' => 'icon-off', 'url' => array('/user/logout'), 'visible' => !Yii::app()->user->isGuest)
								),
							);
						} elseif (Yii::app()->user->checkAccess('Admin DBS')) {
							$menu_user = array('label' => 'Welcome, ' .
								Yii::app()->getModule('user')->user(Yii::app()->user->Id)->fname . ' ' .
								Yii::app()->getModule('user')->user(Yii::app()->user->Id)->lname,
								'url' => '', // doesn't link to any page
								'icon' =>'icon-user',
								'visible'	=> !Yii::app()->user->isGuest,
								'items'	=> array(
									array('label' => 'Options'),
									array('label' => 'My Profile', 'icon' => 'icon-heart', 'url' => array('/user/profile')),
									array('label' => 'Change Password', 'icon' => 'icon-edit', 'url' => array('/user/profile/changepassword')),
									'---',
									array('label' => 'Logout ('.Yii::app()->user->name.')', 'icon' => 'icon-off', 'url' => array('/user/logout'), 'visible' => !Yii::app()->user->isGuest)
								),
							);
						} elseif (Yii::app()->user->checkAccess('Affiliate')) {
							$menu_user = array('label' => 'Welcome, ' .
								Yii::app()->getModule('user')->user(Yii::app()->user->Id)->fname . ' ' .
								Yii::app()->getModule('user')->user(Yii::app()->user->Id)->lname,
								'url' => '', // doesn't link to any page
								'icon' =>'icon-user',
								'visible'	=> !Yii::app()->user->isGuest,
								'items'	=> array(
									array('label' => 'Options'),
									array('label' => 'My Profile', 'icon' => 'icon-heart', 'url' => array('/user/profile')),
									array('label' => 'Change Password', 'icon' => 'icon-edit', 'url' => array('/user/profile/changepassword')),
									'---',
									array('label' => 'Logout ('.Yii::app()->user->name.')', 'icon' => 'icon-off', 'url' => array('/user/logout'), 'visible' => !Yii::app()->user->isGuest)
								),
							);
						}

					} else {
					$menu_user = array();
				}
				$navBarItemsLeft = array(
						$menu_user,
						array('label' => 'Login', 'url' => array('/user/login'), 'visible' => Yii::app()->user->isGuest),
					);
				$this->widget(
						'ext.bootstrap.widgets.TbNavbar',
						array(
							'brand' => '<i class="icon-random"></i> ' . CHtml::encode(Yii::app()->name),
							'brandOptions' => array('style' => 'width:auto;margin-left: 0px;', 'title' => CHtml::encode(Yii::app()->name)),
							'collapse' => true,
							'type'  => 'fixed-top',
							'items' => array(
								/*
								array(
									'class'	=> 'ext.bootstrap.widgets.TbMenu',
									'items'	=> $navBarItems,
								),
								*/
								array(
									'class' => 'ext.bootstrap.widgets.TbMenu',
									'htmlOptions' => array('class' => 'pull-right', 'style' => 'margin-right: 0px !important;'),
									'items' =>  $navBarItemsLeft,
	                            ),
							),
						)
					);
			?>
			<div class="container">
				<?php
					if(isset($this->breadcrumbs)) {
						$this->widget('ext.bootstrap.widgets.TbBreadcrumbs',
							array(
								'homeLink' 	=> CHtml::link('<i class="icon-home"></i> Home', Yii::app()->homeUrl),
								'separator'	=> '»',
								'links'		=> $this->breadcrumbs,
							));
					}
				?>
				<div class="clear"></div>
			</div>
			<div class="container" style="padding-top: 0px !important;">
				<?php echo $content; ?>
			</div>

			<div id="push"></div>
		</div>

		<div id="footer">
			<div class="container">
				<p class="muted credit">Copyright &copy; <?php echo date('Y'); ?> <?php echo Yii::app()->params['companyName']; ?>. All Rights Reserved. </p>
			</div>
		</div>
	</body>
</html>