<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="row-fluid">
	<div class="span10"> 
		<?php echo $content; ?> 
	</div>
	<div class="span2"> 
		<?php 
			$this->widget('ext.bootstrap.widgets.TbMenu', array(
				'items'=>$this->menu,
				'type' => 'list', 
			)); 
		?> 
	</div>
</div>
<?php $this->endContent(); ?>