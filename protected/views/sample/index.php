<?php
/* @var $this SampleController */

$this->breadcrumbs=array(
	'Sample',
);
?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<p>
	You may change the content of this page by modifying
	the file <tt><?php echo __FILE__; ?></tt>.
</p>

<p>This page shows availability of Rights, the Authentication manager modules created by Christoffer Niska <code>cniska@live.com</code>.</p>

<p>If you are admin, please <?php echo CHtml::link('click here', array('/sample/page')); ?>.</p>
<p>If you are authenticated user, please <?php echo CHtml::link('click here', array('/sample/info')); ?>.</p>

<hr/>
<p>You are login using: <strong><?php echo Yii::app()->user->name; ?></strong> with ID (<?php echo Yii::app()->user->Id; ?>)</p>
<p>Are you guest? <code><?php echo (Yii::app()->user->isGuest) ? "Yes" : "No"; ?></code>

<?php if(!Yii::app()->user->isGuest): ?>
<p>User assigned role(s): 
<?php
	$roles = Rights::getAssignedRoles();
	foreach($roles as $role) echo "<code>" . $role->name . "</code>";
?>
</p>
<?php endif; ?>