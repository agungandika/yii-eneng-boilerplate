<?php
/* @var $this SampleController */

$this->breadcrumbs=array(
	'Sample'=>array('/sample'),
	'Page',
);
?>
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<p>
	You may change the content of this page by modifying
	the file <tt><?php echo __FILE__; ?></tt>.
</p>
<p>
	Please note that this page is accessisble by <code>authenticated & admin</code> user only.
</p>
<p>You are login using: <strong><?php echo Yii::app()->user->name; ?></strong> with ID (<?php echo Yii::app()->user->Id; ?>)</p>
<p>You have following roles:
<?php
	$roles = Rights::getAssignedRoles(Yii::app()->user->Id);
	foreach($roles as $role) echo "<code>" . $role->name . "</code>";
?>
</p> 